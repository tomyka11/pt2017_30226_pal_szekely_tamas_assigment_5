//import java.sql.Date;
import java.util.Date;

public class MonitoredData {
	
	public Date startDate;
	public  Date endDate;
	public String activityLabel;
	
	public MonitoredData(){
		super();
	}
	public MonitoredData(java.util.Date d1,java.util.Date d2,String aL){
		this.startDate=d1;
		this.endDate=d2;
		this.activityLabel=aL;
	}
	
	public void setstartDate(Date sD){
		this.startDate=sD;
	}
	public void setendDate(Date eD){
		this.endDate=eD;
	}
	public void setactivityLabel(String aL){
		this.activityLabel=aL;
	}
	
	public Date getstartDate(){
		 return this.startDate;
	}
	public Date getendDate(){
		return this.endDate;
	}
	public String getactivityLabel(){
		return this.activityLabel;
	}
	
	

}
