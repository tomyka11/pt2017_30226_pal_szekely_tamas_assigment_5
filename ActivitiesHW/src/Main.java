import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.*;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Period;
import org.joda.time.Seconds;
public class Main {
	
	public static <T> Predicate<T> distinctByKey(Function<? super T,Object> keyExtractor) {
	    Map<Object,String> seen = new ConcurrentHashMap<>();
	    return t -> seen.put(keyExtractor.apply(t), "") == null;
	}

	public static void main(String[] args) throws FileNotFoundException,IOException{
		
		MonitoredData mD=new MonitoredData();
		List<MonitoredData> list =new ArrayList<MonitoredData>();
		
		File fileTwo=new File("filetwo.txt");
	      FileOutputStream fos=new FileOutputStream(fileTwo);
	          PrintWriter pw=new PrintWriter(fos);
		
		
		
		
		
		
		FileInputStream fstream = new FileInputStream("Activities.txt");
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String strLine;
        while ((strLine = br.readLine()) != null)   {
        	   String[] tokens = strLine.split("		");
        	   Date d1=new Date();
        	   Date d2=new Date();
        	   //Date formatteddate1 = null;
        	   //Date formatteddate2 = null;
        	   DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        	   try{
        	        d1 = df.parse(tokens[0]);
        	        d2=df.parse(tokens[1]);
        	        //formatteddate1 = df.format(d1);
        	    }
        	    catch ( Exception ex ){
        	        System.out.println(ex);
        	    }
        	   String ok = tokens[2].replaceAll("\\s","");
        	   MonitoredData record = new MonitoredData(d1,d2,ok);
        	   list.add(record);
        }
        
        //sub1
        List<Integer> inList=new ArrayList<Integer>();
        
        for(MonitoredData str : list){
        	   inList.add(Integer.valueOf(str.startDate.getDate()));
        	}
        int k=Math.toIntExact(inList.stream().distinct().count());
        
        pw.println("Number of distinct days:" +k);
        
        List<Integer> inList1 =new ArrayList<Integer>();
        inList1=inList.stream().distinct().collect(Collectors.toList());
        
        
      
      //sub2
        List<MonitoredData> l1=new ArrayList<MonitoredData>();
      l1=list.stream().filter(distinctByKey(MonitoredData::getactivityLabel)).collect(Collectors.toList());
      
       
      Map<String,Integer> hm1=new HashMap<String,Integer>();
      for(MonitoredData d:l1){
    	  
    	  int po=Math.toIntExact(list.stream().filter(p-> p.activityLabel.equals(d.activityLabel)).count());
    	  //System.out.println(po);
    	  hm1.put(d.activityLabel,po );
      }
      
      
      
      

          for(Map.Entry<String,Integer> m :hm1.entrySet()){
              pw.println(m.getKey()+"="+m.getValue());
          }

          //pw.flush();
          //pw.close();
          //fos.close();
          
          //sub 3
          
          HashMap<Integer,HashMap<String,Integer>> hm2 =new HashMap<Integer,HashMap<String,Integer>>();
      
          
          for(Integer i:inList1){
        	  
        	 
        	  List<MonitoredData> m10=new ArrayList<MonitoredData>();
        	 m10=list.stream().filter(p ->String.valueOf((p.startDate.getDate())).equals(Integer.toString(i))).collect(Collectors.toList());
        	  
        	 HashMap<String,Integer> hm3 =new HashMap<String,Integer>();
        	 for(MonitoredData n :m10){
        		 int po=Math.toIntExact(m10.stream().filter(p-> p.activityLabel.equals(n.activityLabel)).count());
        		 hm3.put(n.activityLabel, po);
        	 }
        	 for(Map.Entry<String,Integer> m :hm3.entrySet()){
                 pw.println("Day :"+i+m.getKey()+"="+m.getValue());
             }

             
             
        	 hm2.put(i, hm3);
        	 
        	  
          }
          
         
          
          
          //sub 4
          HashMap<String,DateTime> sdt=new HashMap<String,DateTime>();
          
          for (MonitoredData mdf :l1){
        	  List<MonitoredData> fff=list.stream().filter(p-> p.activityLabel.equals(mdf.activityLabel)).collect(Collectors.toList());
        	  int suma=0;
        	  for(MonitoredData d:fff){
        		  int diff = (int)(d.endDate.getTime() - d.startDate.getTime());
        		  diff=diff/60000;
        		  suma=suma+diff;
        		  
        	  }
        	  Predicate<Integer> greaterthenTenHour=x->x>10;
        	  if (greaterthenTenHour.test(suma/60))
        		  pw.println("It takes more then 10 hours" +" "+mdf.activityLabel);
        	  pw.println(mdf.activityLabel+"="+suma + " minutes");
        	  DateTime sum=new DateTime(suma);
        	  sdt.put(mdf.activityLabel,sum);
        	  
        	 // DateTime diff = new DateTime(mdf.endDate.getTime() - mdf.startDate.getTime());
        	 // System.out.println(diff.getMinuteOfDay());
          }
          pw.flush();
          pw.close();
          fos.close();
      
			
	}
	
	}
		
	
	
